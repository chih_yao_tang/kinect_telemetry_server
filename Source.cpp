
#include <opencv.hpp>
#include <highgui.hpp>
#include <iostream>
#include <fstream>
#include <set>
#include <vector>

#define _WEBSOCKETPP_CPP11_FUNCTIONAL_
#define _WEBSOCKETPP_CPP11_MEMORY_ 
#define _WEBSOCKETPP_CPP11_CHRONO_

#include <websocketpp/config/asio_no_tls.hpp>
#include <websocketpp/server.hpp>

#include "./rapidjson/document.h"
#include "./rapidjson/writer.h"
#include "./rapidjson/stringbuffer.h"
#include "./rapidjson/prettywriter.h"
#include "KinectSource.h"

std::vector<KinectSource*> kinect_instance;


class telemetry_server {
public:
	typedef websocketpp::connection_hdl connection_hdl;
	typedef websocketpp::server<websocketpp::config::asio> server;
	typedef websocketpp::lib::lock_guard<websocketpp::lib::mutex> scoped_lock;

	telemetry_server() : m_count(0) {
		// set up access channels to only log interesting things
		m_endpoint.clear_access_channels(websocketpp::log::alevel::all);
		m_endpoint.set_access_channels(websocketpp::log::alevel::access_core);
		m_endpoint.set_access_channels(websocketpp::log::alevel::app);

		// Initialize the Asio transport policy
		m_endpoint.init_asio();

		// Bind the handlers we are using
		using websocketpp::lib::placeholders::_1;
		using websocketpp::lib::bind;
		m_endpoint.set_open_handler(bind(&telemetry_server::on_open, this, _1));
		m_endpoint.set_close_handler(bind(&telemetry_server::on_close, this, _1));
		
	}

	void run(uint16_t port) {
		std::stringstream ss;
		ss << "Running telemetry server on port " << port ;
		m_endpoint.get_alog().write(websocketpp::log::alevel::app, ss.str());

		

		// listen on specified port
		m_endpoint.listen(port);

		// Start the server accept loop
		m_endpoint.start_accept();

		// Set the initial timer to start telemetry
		set_timer();

		// Start the ASIO io_service run loop
		try {
			m_endpoint.run();
		}
		catch (websocketpp::exception const & e) {
			std::cout << e.what() << std::endl;
		}
	}

	void set_timer() {
		m_timer = m_endpoint.set_timer(
			1,
			websocketpp::lib::bind(
			&telemetry_server::on_timer,
			this,
			websocketpp::lib::placeholders::_1
			)
			);
	}

	void on_timer(websocketpp::lib::error_code const & ec) {
		if (ec) {
			// there was an error, stop telemetry
			m_endpoint.get_alog().write(websocketpp::log::alevel::app,
				"Timer Error: " + ec.message());
			return;
		}

		std::stringstream val;
		
		val << "count is " << m_count++;
		// Broadcast count to all connections
		con_list::iterator it;
		for (it = m_connections.begin(); it != m_connections.end(); ++it) {
			for (vector<KinectSource*>::iterator vit = kinect_instance.begin(); vit != kinect_instance.end(); vit++)
			{
				rapidjson::StringBuffer* buffer = NULL; 
				(*vit)->get_kinect_data(buffer);
				cout << (*vit)->buffer.GetSize() << endl;
				m_endpoint.send(*it, (*vit)->buffer.GetString(), websocketpp::frame::opcode::TEXT);
				//m_endpoint.send(*it, val.str(), websocketpp::frame::opcode::text);
			}
		}

		// set timer for next telemetry check
		set_timer();
	}

	void on_message(connection_hdl hdl){
	
	}


	void on_open(connection_hdl hdl) {
		m_connections.insert(hdl);
	}

	void on_close(connection_hdl hdl) {
		m_connections.erase(hdl);
	}
private:
	typedef std::set<connection_hdl, std::owner_less<connection_hdl>> con_list;

	server m_endpoint;
	con_list m_connections;
	server::timer_ptr m_timer;

	// Telemetry data
	uint64_t m_count;
};

int main(int argc, char* argv[]) {
	telemetry_server s;
	openni::OpenNI::initialize();
	bool IS_LINE_STREAM = false;
	uint16_t port = 9002;
	openni::Array<openni::DeviceInfo> deviceList;
	if (argc == 1) {
		std::cout << "Usage: telemetry_server [file_dir] [...]" << std::endl;
	}

	if (IS_LINE_STREAM)
	{
		openni::OpenNI::enumerateDevices(&deviceList);
		for (int i = 0; i < deviceList.getSize(); i++)
		{
			KinectSource* k = new KinectSource(deviceList[i].getUri(), i, STATUS_LIVE_STREAM);
			kinect_instance.push_back(k);
		}
	}
	else if (argc >= 2) {
		for (int i = 0; i < argc - 1; i++)
		{
			kinect_instance.push_back(new KinectSource(argv[i + 1], i, STATUS_NOT_LIVE_STREAM));
		}
	}

	s.run(port);
	return 0;
}