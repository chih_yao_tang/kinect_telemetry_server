#include "KinectSource.h"
#include <fstream>
#include <string>
#include <opencv.hpp>
#include <highgui.hpp>
void KinectSource::get_kinect_data(rapidjson::StringBuffer* buffer_out)
{
	cout << "*1. buffer size : " << buffer.GetSize() << endl;
	buffer.Clear();
	openni::VideoFrameRef depthFrame;
	rapidjson::Document::AllocatorType& localAllocator = d->GetAllocator();
	r->getFrame(&depthFrame);
	cv::Mat image_depth(depthFrame.getHeight(), depthFrame.getWidth(), CV_16UC1, (void*)depthFrame.getData());
	unsigned short tmp_data;

	if (frame_data.Size() < 640 * 480)
	{
		for (int i = 0; i < 480 * 640; i++)
		{
			frame_data.PushBack(0, d->GetAllocator());
			//std::cout << "* i : " << i << endl;
		}
	}
	
	// Get each pixel data from the frame
	for (int i = 0; i < 480; i++)
	{
		for (int j = 0; j < 640; j++)
		{
			//tmp_data = tmp_data > 800 && tmp_data <= 4000 ? tmp_data : 0;
			//cout << i << ", " << j << ", " << tmp_data << endl;
			frame_data[j + i * 640] = (unsigned short)image_depth.at<unsigned short>(i, j);
		}
	}

	// Add frame data to json package
	// NOTE : Use swap to avoid memory leak 
	//cout << "*1 frame_data.Size() : " << frame_data.Size() << endl;
	if (d->HasMember("data"))
	{	
		(*d)["data"].Swap(frame_data_2); // Swap data into json document
		(*d)["data"].Swap(frame_data);
		frame_data.Swap(frame_data_2);
	}
	else
	{
		d->AddMember("data", frame_data, localAllocator);// frame_data == NULL
		frame_data.Swap(frame_data_2); // frame_data_2 == NULL
	}
	

	if (d->HasMember("frame_id"))
		(*d)["frame_id"] = frame_ID;
	else
		d->AddMember("frame_id", frame_ID, localAllocator);

	
	rapidjson::Writer<rapidjson::StringBuffer >* writer = new rapidjson::Writer<rapidjson::StringBuffer >(buffer);
	d->Accept(*writer);
	cout << "* frame_ID : " << frame_ID << endl;
	frame_ID++;
	cout << "*2. buffer size : " << buffer.GetSize() << endl;
	buffer_out = &buffer;
	cout << "*3. buffer size : " << buffer_out->GetSize() << endl;
}

void KinectSource::set_camera_ID(int id)
{
	this->camera_ID = id;
}



void KinectSource::wrap_text_file_to_Json()
{
	rapidjson::Document::AllocatorType& localAllocator = d->GetAllocator();

	rapidjson::Value mR;
	mR.SetArray();
	std::fstream fp;
	fp.open(std::string(device_PID) + "_mR.txt", ios::in);
	
	for (int k = 0; k < 9; k++)
	{
		string element("1");
		if (fp.is_open())
		{
			fp >> element;
		}

		mR.PushBack(rapidjson::Value(atof(element.c_str())), localAllocator);
	}
	
	fp.close();

	rapidjson::Value mT;
	mT.SetArray();
	fp.open(std::string(device_PID) + "_mT.txt", ios::in);
	//cout << "* open : " << std::string(device_PID) + "_mT.txt" << endl;
	for (int k = 0; k < 3; k++)
	{
		string element("1");
		if (fp.is_open())
		{
			fp >> element;
		}
		//cout << atof(element.c_str()) << endl;
		mT.PushBack(rapidjson::Value(atof(element.c_str())), localAllocator);
	}
	fp.close();

	rapidjson::Value mAcc;
	mAcc.SetArray();
	//cout << "* open : " << std::string(device_PID) + "_mAcc.txt" << endl;
	fp.open(std::string(device_PID) + "_mAcc.txt", ios::in);
	for (int k = 0; k < 9; k++)
	{
		string element("1");
		if (fp.is_open())
		{
			fp >> element;
		}
		//cout << atof(element.c_str()) << endl;
		mAcc.PushBack(rapidjson::Value(atof(element.c_str())), localAllocator);
	}
	fp.close();

	rapidjson::Value mOrien;
	mOrien.SetArray();
	//cout << "* open : " << std::string(device_PID) + "_mOrien.txt" << endl;
	fp.open(std::string(device_PID) + "_mOrien.txt", ios::in);
	for (int k = 0; k < 9; k++)
	{
		string element("1");
		if (fp.is_open())
		{
			fp >> element;
		}
		//cout << atof(element.c_str()) << endl;
		mOrien.PushBack(rapidjson::Value(atof(element.c_str())), localAllocator);
	}
	fp.close();

	rapidjson::Value ransac_d;
	ransac_d.SetArray();
	//cout << "* open : " << std::string(device_PID) + "_ransac_d.txt" << endl;
	fp.open(std::string(device_PID) + "_ransac_d.txt", ios::in);
	for (int k = 0; k < 1; k++)
	{
		string element("1");
		if (fp.is_open())
		{
			fp >> element;
		}
		//cout << atof(element.c_str()) << endl;
		ransac_d.PushBack(rapidjson::Value(atof(element.c_str())), localAllocator);
	}
	fp.close();

	rapidjson::Value mVertex;
	mVertex.SetArray();
	cout << "* open : " << std::string(device_PID) + "_vertex.txt" << endl;
	fp.open(std::string(device_PID) + "_vertex.txt", ios::in);
	for (int k = 0; k < 3; k++)
	{
		string element("1");
		if (fp.is_open())
		{
			fp >> element;
		}
		//cout << atof(element.c_str()) << endl;
		mVertex.PushBack(rapidjson::Value(atof(element.c_str())), localAllocator);
	}
	fp.close();

	d->AddMember("ransac_d", ransac_d, localAllocator);
	d->AddMember("matrix_mOrien", mOrien, localAllocator);
	d->AddMember("matrix_acc", mAcc, localAllocator);
	d->AddMember("matrix_r", mR, localAllocator);
	d->AddMember("matrix_t", mT, localAllocator);
	d->AddMember("vertex", mVertex, localAllocator);
	d->AddMember("type", rapidjson::Value("wait"), localAllocator);
	d->AddMember("camera", rapidjson::Value(camera_ID), localAllocator);

	//cout << "matrix_r : " << (*d)["matrix_r"].GetString() << endl;
	//cout << "matrix_t : " << (*d)["matrix_t"].GetString() << endl;
	//cout << "type : " << d["type"].GetString() << endl;
	//cout << "camera : " << d["camera"].GetInt() << endl;
}