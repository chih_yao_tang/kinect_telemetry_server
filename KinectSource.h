#include "./rapidjson/document.h"
#include "./rapidjson/writer.h"
#include "./rapidjson/stringbuffer.h"
#include "./rapidjson/prettywriter.h"
#include <random>
#include "ONIReader.h"

using namespace std;

enum{
	STATUS_NOT_LIVE_STREAM,
	STATUS_LIVE_STREAM
};

class KinectSource
{
public:
	KinectSource(const char* file_dir, int id, bool stream_status) : 
		IS_LiveStream(stream_status),
		camera_ID(id),
		frame_ID(0),
		frame_data(rapidjson::kArrayType),
		frame_data_2(rapidjson::kArrayType)
		//buffer(new rapidjson::StringBuffer)
	{
		device_PID = file_dir;
		if (IS_LiveStream)
		{
			r = new ONIReader(string(device_PID).c_str(), STATUS_LIVE_STREAM);
			
		}
		else
			r = new ONIReader(string(device_PID + ".oni").c_str(), STATUS_NOT_LIVE_STREAM);
		d = new rapidjson::Document();
		d->SetObject();
		wrap_text_file_to_Json();
		// Initialize frame_data size
		if (frame_data.Size() < 640 * 480)
		{
			for (int i = 0; i < 480 * 640; i++)
			{
				frame_data.PushBack(0, d->GetAllocator());
				frame_data_2.PushBack(0, d->GetAllocator());
				//std::cout << "* i : " << i << endl;
			}
		}
	};

	void wrap_text_file_to_Json();
	void set_camera_ID(int id);
	void get_kinect_data(rapidjson::StringBuffer* buffer_out);
	void get_kinect_data_array(rapidjson::StringBuffer* buffer_out);;
	~KinectSource();

private:
	bool IS_LiveStream;
	int camera_ID;
	int frame_ID;
	std::string device_PID;
	ONIReader* r;
	rapidjson::Document* d;
	rapidjson::Value frame_data;
	rapidjson::Value frame_data_2;
public:
	rapidjson::StringBuffer buffer;

};